#!/bin/bash

export HOME=/home/displayuser
export XDG_RUNTIME_DIR=/tmp/displayserver/.xdg
export DISPLAY=:0
export XAUTHORITY=/tmp/displayserver/.xdg/.Xauthority
export WAYLAND_DISPLAY=wayland-0
export PULSE_SERVER=unix:/run/pulse/pulseaudio.socket

rm -f /tmp/chromium/arch/Singleton*

#sudo -E -u displayuser -- chromium --in-process-gpu  --enable-oop-rasterization --enable-gpu-rasterization --ignore-gpu-blocklist --enable-accelerated-video-decode --disable-gpu-sandbox https://static.steffend.me/Jabberwocky1080p.mp4
sudo -E -u displayuser -- chromium-browser --autoplay-policy=no-user-gesture-required --no-user-gesture-required --disable-site-isolation-trials --test-type $@
#chromium $@


#sleep infinity
