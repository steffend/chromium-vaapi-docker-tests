# Information

These containers are built to run on a [balenaOS device](https://www.balena.io). Therefore running on plain docker might be a little bit different.

I'm using the following command to start the containers:

```
balena run --name chrome --rm --net=host -v 1737230_sharedTmp:/tmp/displayserver -v /dev:/dev -v 1737230_pulse:/run/pulse -v 1737230_chromiumData:/tmp/chromium/ --privileged chrometest:latest --in-process-gpu --enable-oop-rasterization --enable-gpu-rasterization --ignore-gpu-blocklist --ignore-gpu-blacklist --enable-accelerated-video-decode --kiosk --user-data-dir=/tmp/chromium/arch --disable-dev-shm-usage --use-gl=egl --enable-logging=stderr --loglevel=0 --vmodule=vaapi_wrapper=4,vaapi_video_decode_accelerator=4 --no-sandbox "https://anubis.steffend.me/vid/bbb_sunflower_2160p_60fps_normal.mp4"
```

after building them using

```
root@7343b93:/tmp/alpine# balena build -t chrometest .
```

This obviously requires named volumes `1737230_sharedTmp`, `1737230_pulse` and `1737230_chromiumData` which are provided by other containers.
On standard Docker one should be able to run this using the following command (untested):

```
docker run --name chrome --rm --net=host -v /tmp/.X11-unix:/tmp/.X11-unix -v /dev:/dev -v /tmp/chromium_data:/tmp/chromium/ --privileged chrometest:latest --in-process-gpu --enable-oop-rasterization --enable-gpu-rasterization --ignore-gpu-blocklist --ignore-gpu-blacklist --enable-accelerated-video-decode --kiosk --user-data-dir=/tmp/chromium/arch --disable-dev-shm-usage --use-gl=egl --enable-logging=stderr --loglevel=0 --vmodule=vaapi_wrapper=4,vaapi_video_decode_accelerator=4 --no-sandbox "https://anubis.steffend.me/vid/bbb_sunflower_2160p_60fps_normal.mp4"
```

Make sure that the symbolic link for the X11 socket is not created by modifying the docker files.

# Running on balena

[Balena](https://www.balena.io/cloud/) is super cool for running containers on embedded devices. I've included a skeleton (see docker-compose.yml) that represents what I'm doing with the project.


# Observations

Currently, VA-API acceleration does not seem to work on Alpine Linux despite using the [same patches](https://git.alpinelinux.org/aports/tree/community/chromium/APKBUILD) as the [ungoogled-chromium](https://github.com/ungoogled-software/ungoogled-chromium-archlinux/blob/master/PKGBUILD) un Arch uses.

## Running chromium with VA-API support:

When having chromium compiled with `use_vaapi=true` you should be able to run chromium with VA-API support and debug logging by adding the following flags when launching:

```--ignore-gpu-blacklist --enable-accelerated-video-decode --enable-logging=stderr --loglevel=0 --vmodule=vaapi_wrapper=4,vaapi_video_decode_accelerator=4```

For me (using an Intel 7th-Gen NUC with UHD Graphics 600) I'm also using `--use-gl=egl` as all other GL implementations are having bad tearing issues...

## Symptoms

When running on Alpine VA-API does not seem to get initialized at all. No log entries. Empty table on chrome://gpu. What we should see is something like this:

```
[9:26:1117/110935.547324:VERBOSE2:vaapi_video_decode_accelerator.cc(204)] Initialize(): Initializing VAVDA, profile: h264 high
[9:171:1117/110935.561854:VERBOSE2:vaapi_video_decode_accelerator.cc(479)] DecodeTask(): Decoder requesting a new set of surfaces
[9:26:1117/110935.564581:VERBOSE2:vaapi_video_decode_accelerator.cc(569)] InitiateSurfaceSetChange():  |requested_num_pics_| = 6; |requested_num_reference_frames_| = 7
[9:26:1117/110935.567455:VERBOSE2:vaapi_video_decode_accelerator.cc(635)] TryFinishSurfaceSetChange(): Requesting 6 pictures of size: 3840x2160 and visible rectangle = 0,0 3840x2160
```

Running other programs that can use VA-API works on Alpine (same container as chromium), e.g. mpv:

```
bash-5.0$ mpv --hwdec=vaapi https://anubis.steffend.me/vid/bbb_sunflower_2160p_60fps_normal.mp4
 (+) Video --vid=1 (*) (h264 3840x2160 60.000fps)
 (+) Audio --aid=1 (*) (mp3 2ch 48000Hz)
     Audio --aid=2 (*) (ac3 6ch 48000Hz)
File tags:
 Artist: Blender Foundation 2008, Janus Bager Kristensen 2013
 Comment: Creative Commons Attribution 3.0 - http://bbb3d.renderfarming.net
 Composer: Sacha Goedegebure
 Genre: Animation
 Title: Big Buck Bunny, Sunflower version
Using hardware decoding (vaapi).
VO: [gpu] 3840x2160 vaapi[nv12]
AO: [pulse] 48000Hz stereo 2ch float
AV: 00:00:03 / 00:10:34 (0%) A-V:  0.000 Dropped: 4 Cache: 143s/131MB


Exiting... (Quit)
```