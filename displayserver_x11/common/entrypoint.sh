#!/bin/bash

rm /tmp/.X0-lock &>/dev/null || true
rm -rf /tmp/.X11-unix

echo "displayserver starting up with hostname $HOSTNAME"

export DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket

# initialize xdg + x11 directories
bash ./init-dirs.sh
export XDG_RUNTIME_DIR="/tmp/.xdg"

su -c "startx ./display-init.sh" displayuser
