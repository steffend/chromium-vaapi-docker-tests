#!/bin/bash

echo "cleaning shared tmp"
shopt -s dotglob
rm -rf /tmp/*
shopt -u dotglob

echo "creating new directories"
mkdir /tmp/.xdg

chmod 0700 /tmp/.xdg
chown -R displayuser:displayuser /tmp/.xdg
