#!/bin/bash

echo "hello from display init"
export DISPLAY=:0

# we need this in the other containers (or disable xauth completely...)
cp /home/displayuser/.Xauthority /tmp/.Xauthority

xset s off
xset -dpms
xset s noblank

# hide mouse pointer
unclutter &

n=0
until [ "$n" -ge 5 ]
do
  if [[ "$DEBUG_I3" == 1 ]]; then
    echo "Starting i3 with debug output..."
    i3 -c i3.cfg -d all
  else
    echo "Starting i3..."
    i3 -c i3.cfg
  fi

  echo "i3 crashed or exited. Waiting 5 seconds and retrying..."
  n=$((n+1))
  sleep 5
done

echo "giving up... Maybe docker can fix this?"
